---
title: Gestire il cambiamento
---

Utilizzare  nuovi strumenti e cambiare le abitudini: un’ambizione ad alto rischio
{: .slogan-subtitle }

## Evitare il fallimento

Inutile girarci intorno: imporre nuovi strumenti (liberi o meno) ai vostri collaboratori-trici, è una scommessa pericolosa! Senza una pianificazione precisa e trasparente il fallimento è assicurato, la strategia da adottare si basa su decisioni condivise con tutti (o quasi). Le nostre convinzioni sulla bontà della scelta del software libero nulla possono contro l’assenza di condivisione del cambiamento. I software liberi non sono solo dei beni comuni disponibili per tutti e tutte, utilizzarli significa una presa di coscienza della nostra partecipazione ai beni comuni digitali, e per questo il loro utilizzo deve essere condiviso e non imposto.

## Diluire le tensioni

Moltissimi fattori, giustificati o meno, inibiscono l’adozione di nuovi modi di agire. Si può pensare che un software proprietario sia più efficace, si adatti meglio alle nostre necessità e sia più facile da imparare. A volte le convinzioni sono più insormontabili delle difficoltà tecniche oggettive.

Tutte queste incognite che ogni utente porta con se devono essere affrontate con grandissima attenzione e delicatezza.
{: .slogan-contenu }

Inutile negare che alcuni software liberi manchino, a volte, di ergonomia o presentino interfacce di difficile comprensione. Non è sempre facile convincere i nostri collaboratori-trici che i valori di libertà, di condivisione e solidarietà sui quali si fondano, valgano una interfaccia a volte scomoda. Dovrete acquistare un server o un hosting di server, o affittare l'hosting del servizio da un'istanza gestita da una terza parte di fiducia.

Nello stesso modo l’adozione dei software liberi implica sovente l’utilizzo di nuovi protocolli, nuovi formati e concetti fino ad ora sconosciuti dagli utenti : questo ci porta a dover imparare nuove cose con diversi livelli di apprendimento. Per questo non dobbiamo soffermarci al solo coinvolgimento dei nostri collaboratori-trici ma sopratutto discutere con loro una fase di transizione tecnica e organizzativa.

Non imponete mai un software libero: non si calpesta la libertà di pensiero!
{: .slogan-contenu }

## Strategie di aggiramento

Prima di ogni cambiamento è importante analizzare il modus operandi personale. Non possiamo raccogliere in uno schema le metodologie di lavoro, in quanto non sempre possono essere riassunte in qualche parola, serve una concertazione aperta e trasparente e una supervisione attenta dei futuri utenti dei software liberi.

Ad esempio, in un flusso di produzione, gli utenti possono avere comportamenti che non corrispondono al normale uso del software in uso. La mancanza di formazione o di esperienza, oppure per azioni non implementate nella concezione del software, gli utenti adottano delle strategie per aggirare i limiti e raggiungere il risultato. Come possiamo interpretare queste strategie e integrarle al meglio nel nuovo sistema a base di software libero che vogliamo creare?
{: .encart }

### L’interoperabilità è buona cosa, ma richiede nuove conoscenze.

Abbiamo citato i **formati**. Un formato libero o aperto viene definito interoperabile perché le sue specifiche sono conosciute e la loro interpretazione può essere declinata in moltissime varianti.

Per questa ragione il formato OpenDocument ( ad esempio il .odt che creiamo con il pacchetto LibreOffice) viene raccomandato in numerose pubbliche amministrazioni ed è definito dalla norma ISO 26300 al contrario di formati proprietari come il .doc di Microsoft.

Utilizzare questi formati in un ambiente dove i software proprietari impongono metodi e formati richiede un grande spirito di adattamento.<br>
Ad esempio, se una pubblica amministrazione invia alla vostra associazione un questionario in formato .docx (diversamente da quanto dovrebbe fare) sarà possibile aprirlo e salvarlo con LibreOffice, ma dovrà essere posta particolare attenzione durante la conversione del formato, problema che prima del passaggio al software libero non si poneva.
{: .encart }

## Sei la persona giusta?


Appassionato di software libero, il tuo computer gira sotto GNU/Linux, amici e colleghi ti tempestano di domande e di richieste di aiuto, ti fai in quattro per loro e sacrifichi molto tempo libero per aiutarli.

Tutto questo fa di te la persone giusta per organizzare la transizione verso il software libero di tutta la struttura di una organizzazione, con i suoi volontari e i suoi dipendenti?

Forse sì, a patto di elaborare un piano accettato da tutti e soprattutto **senza prendere decisioni da solo**.

- **Il «fattore bus»** (calcolo del rischio dovuto alla mancata condivisione di informazioni): il tecnico che ha installato i software liberi su tutti i pc della segreteria, sfortunatamente è appena stato investito da un bus e non potrà aiutarvi nei prossimi aggiornamenti. Risultato, utenti e collaboratori costretti a utilizzare versioni obsolete dei software perché più nessuno sa cosa fare.
- **L’emarginazione:** gran parte dei volontari della vostra associazione decide di usare [Mattermost](../Comunicare/mattermost.md) per le chat, senza badare agli altri collaboratori che non hanno tempo di apprendere o installare il software. Si crea una situazione discriminatoria e i collaboratori si isolano o lasciano l’associazione.



## Valutare i costi

Ogni cambiamento implica dei costi. Possono essere umani o finanziari e si valutano nel tempo. Se decidete di utilizzare servizi etici in cloud, dovrete acquistare un server o un hosting di server, o affittare l'hosting del servizio su un'istanza gestita da una terza parte di fiducia ( come i CHATONS).



Fino ad ora la vostra associazione poteva usufruire del servizio gratuito DropBox, da adesso non solo dovrete pagare un host per la vostra istanza Nextcloud, ma dovrete organizzare la migrazione, fare copie di sicurezza, importare i documenti e ri catalogarli, dovrete insegnare ai vostri collaboratori-trici ad utilizzare la nuova interfaccia…<br>
Insomma prevedete anche i costi in termini di tempo.
{: .encart }

Le libertà che offrono i software liberi implica degli sforzi che la vostra organizzazione deve saper anticipare. Attenzione: le dimensioni della vostra associazione non presuppongono in alcun modo le capacità ad effettuare una transizione verso il software libero.


[![formats-ouverts](../img/formats_ouverts.jpg){: .illustration }](https://www.april.org/formats-ouverts-pour-quoi-faire){target=_blank}
Lo scambio di file è problematico se non si tiene conto della questione dell'interoperabilità dei formati. Non tutti usano lo stesso software: è una libertà, non un vincolo.  
Estratto dal poster [« Formati aperti, a che serve? »](https://www.april.org/formats-ouverts-pour-quoi-faire), April, 2012.  
Grafica : Antoine Bardelli      
Licenza [Art Libre](http://artlibre.org/)*
{: .figure }
