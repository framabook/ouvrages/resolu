---
title: I beni comuni digitali
---

Partecipiamo insieme alla creazione di beni comuni
{: .slogan-subtitle }


Un bene comune è una risorsa **condivisa e gestita** da una comunità secondo regole d'uso e di governance definite collettivamente.

La gestione dei beni comuni si basa sulla **condivisione** e sulla **libera circolazione** di queste risorse, materiali e immateriali.

Partecipare ad un bene comune è contribuire alla produzione o al mantenimento di una risorsa. Che viene resa accessibile a tutti.

Come membro della comunità, ognuno ha voce in capitolo nelle decisioni riguardanti le condizioni d'uso, l'organizzazione e la governance della risorsa.



Poiché il potere è distribuito tra tutti gli utenti, l'uso della risorsa non può essere riappropriato esclusivamente da un membro: nessuna decisione arbitraria, né responsabilità esclusiva.
{: .encart }


Questo modello sociale rende possibile la creazione collettiva di valore, le cui conseguenze positive sono direttamente nell'interesse della comunità e dei suoi membri. È un modello basato sui valori della contribuzione, della condivisione della conoscenza e della cooperazione.
{: .slogan-contenu }

## Alcuni esempi di beni comuni digitali

**Wikipédia** : questa enciclopedia collaborativa sostiene l'accesso universale alla conoscenza. Attraverso il suo funzionamento, gli utenti possono contribuire con le loro conoscenze a costruire una delle enciclopedie più complete al mondo e a tradurre articoli in più di 300 lingue.

**OpenStreetMap** è un database cartografico al quale tutti possono contribuire. Questa collaborazione permette di creare una mappa del mondo precisa e rispondente alle esigenze di tutti: ognuno aggiunge la propria esperienza del territorio e contribuisce così ad arricchire le molteplici mappe a disposizione di tutti. 

Entrambi questi strumenti utilizzano software libero, che sono essi stessi beni comuni: tutti possono usarli e partecipare al loro miglioramento.


## Cosa ci guadagniamo?

**Accesso universale** - L'accesso a un bene comune digitale è aperto a voi, la scelta di utilizzarlo o meno è vostra. Dare accesso al maggior numero di persone significa ricevere un maggior numero di idee.

**Distribuzione del potere** - Come utente, puoi partecipare alle decisioni sull'uso della risorsa e sull'organizzazione della comunità. Le regole sono trasparenti e si evolvono attraverso decisioni collettive. 

**Robustezza e sostenibilità** - Il modello comune produce risorse complesse e sofisticate. Soprattutto, la loro apertura permette a tutti di mantenerli e migliorarli; a differenza di una risorsa gestita da un singolo attore, la sostenibilità di un commons è garantita dall'evoluzione della sua comunità. 

**Potenziamento** - L'uso di una comunità risponde inizialmente a un'esigenza, ma la possibilità di contribuire ad essa porta anche potenziamento: si impara il know-how, si scoprono nuovi usi. In altre parole, acquisiamo capacità che aumentano il nostro potere d'azione.

**Coesione sociale** - Un bene comune non è nulla senza una comunità vivente che lo circonda: incontri e raggruppamenti che permettono di prendere decisioni collettive intrecciano solide relazioni tra gli utenti. Investire in una comunità ti porta grandi sorprese; e se non ti piace più, sei libero!


## Solidarietà e collaborazione

Gli strumenti digitali proprietari mantengono gli utenti all'oscuro del loro funzionamento. Al contrario, tutte le conoscenze accumulate nel software libero vengono messe in comune.

Così, nessuno deve partire da zero se la soluzione è già disponibile da qualche parte e gli strumenti possono evolvere per soddisfare al meglio le esigenze!

## Emancipazione

Partecipare alle aree comuni significa dare a se stessi l'opportunità di sviluppare le proprie capacità per costruire l'autonomia, migliorare il proprio ambiente e le proprie condizioni di vita. L'accesso ai dati e l'apprendimento del saper fare offrono opportunità di azione. Questo **libero accesso** deve quindi essere garantito per garantire che tutti siano in grado di decidere e agire indipendentemente da qualsiasi controllo arbitrario.

## Condivisione e inclusione sociale

Che si tratti di informazioni, opere, conoscenze, risorse... i beni comuni sono vettori di inclusione sociale perché permettono a tutti di accedervi liberamente. La creatività e le iniziative individuali al servizio dell'interesse collettivo emergono così più facilmente. Questo apre uno spazio più ampio per la **mobilitazione collettiva**, che è guidata da una **solidarietà intelligente**.


## Economia sociale e solidale: lavoriamo insieme!


Cortocircuiti, sviluppo sostenibile, sistemi di scambio alternativi sono esempi tra le tante attività associative che lavorano per la prosperità della gente comune. Allo stesso modo, i beni comuni digitali non sono solo linee di codice accessibili e regole trasparenti per la modifica: esistono grazie all'azione di una comunità. È qui che l'impegno delle associazioni è fondamentale: per preservare le risorse digitali da riappropriazioni esclusive e dall'essere rinchiuse, ognuno ha un ruolo da svolgere nel sostenere il modello di gestione che vuole per i suoi strumenti digitali.


## Il coinvolgimento

Gli strumenti digitali sono utili per la mobilitazione e facilitano il «Fare le cose insieme»: valorizzare e diffondere le proprie azioni. La tecnologia digitale rende queste azioni collettive ancora più efficaci e apre nuove aree di visibilità. L'impegno associativo viene così rafforzato.

Agire efficacemente per la propria associazione utilizzando strumenti digitali gratuiti significa dispiegare il proprio impegno **partecipando alla difesa dei beni comuni digitali e quindi delle libertà** (in particolare la libertà di espressione).
{: .slogan-contenu }

## Adesso tocca a te!

Usare il software libero per la sua azione associativa significa difendere i valori associati al bene comune e sostenere un modello di società in cui la tecnologia digitale è al servizio dei cittadini e dell'interesse generale.

Rafforzate la vostra azione facendo parte della transizione verso gli strumenti digitali liberi!
{: .slogan-contenu }


Anche voi, partecipate all'elaborazione di beni comuni:

- utilizzate software libero, date la vostra opinione ai loro contributori,
- Contribuite con le vostre conoscenze e competenze agli strumenti esistenti,
- distribuite i vostri contenuti sotto una licenza libera, come Creative Commons o Art Libre, al fine di incoraggiare la circolazione delle vostre idee proteggendo al contempo i vostri diritti d'autore,
- informate anche i vostri soci sui mezzi a loro disposizione per partecipare all'organizzazione e allo sviluppo della vostra attività associativa,
- Partecipate alle traduzioni dei contenuti che vi interessano,
- fare dei tutorial per condividere il vostro saper fare.



## La lotta contro le disuguaglianze

La monopolizzazione degli strumenti digitali da parte di qualcuno impedisce il libero accesso alle risorse digitali. Il modello proprietario alimenta questa dinamica di privatizzazione delle risorse, che aumenta le disuguaglianze di potere e i divari economici tra gli attori della società. Al contrario, i valori del FOSS sono allineati con quelli dell'**Economia Sociale e Solidale**.

## Non dimenticare

Il copyright si applica sempre, indipendentemente dall'opera. Ricordatevi di non utilizzare contenuti per i quali non conoscete la licenza e attribuite sempre la paternità del contenuto che utilizzate.

Quando le abitudini vengono cambiate, è necessario predisporre un supporto per l'abbandono di alcuni usi e l'adozione di nuovi. Di fronte a una nuova modalità di consumo e di partecipazione, non è sempre facile sapere da dove cominciare! Quindi non dimenticate di formare gli utenti ai nuovi strumenti. Può essere utile nominare come referente qualcuno che ha avuto il tempo di capire lo strumento e che sarà in grado di aiutare gli altri se necessario.

## Alcuni collegamenti

[wikipedia.org](http://wikipedia.org/)

[openstreetmap.org](http://openstreetmap.org/)

[creativecommons.org](http://creativecommons.org/)

[artlibre.org](http://artlibre.org/)

[framabook.org](http://framabook.org/)
