---
title: Decentralizzazione
---

Una sfida per tutta l’Economia Sociale e Solidale
{: .slogan-subtitle }

## Un po’ di teoria

Per capire le sfide della decentralizzazione, è necessario distinguere Internet e il web. Il primo è una interconnessione di reti (reti governative, reti private, reti pubbliche, ecc.) e si basa tanto su infrastrutture tecniche che sull’uso di protocolli che permettono lo scambio di informazioni tra macchine e reti. Il secondo è anche noto come la «rete mondiale» (*world wide web*); è uno dei livelli applicativi di Internet che permettono la leggibilità di contenuti che rimangono collegati tra loro, in particolare grazie al protocollo HTML (è quanto perlopiù vedete quando «navigate nella rete»).


In un mondo numerico ideale, non si tratta affatto di centralizzazione dell’informazione o di concentrazione delle risorse tecniche: tutto dovrebbe funzionare in maniera autonoma, vale a dire indipendentemente da un operatore senza il quale gli scambi sarebbero impossibili.
{: .slogan-contenu }

Di solito, con Internet, anche se uno dei nodi della rete si guasta, i pacchetti d’informazione possono comunque circolare: passano attraverso altri percorsi mantenuti da una pluralità di attori.

## Centralizzare, concentrare

Si parla di centralizzazione o di concentrazione quando uno di questi attori rende obbligatorio il passaggio di contenuti informativi attraverso protocolli o formati che solo lui conosce o autorizza (centralizzazione forzata dell’informazione) o attraverso sue macchine e suoi sistemi tecnici (concentrazione delle risorse). Potrebbe essere ad esempio un governo repressivo che concentra le comunicazioni della propria rete nazionale in modo da censurarle. Ma potrebbe anche essere un operatore commerciale che cerca di mantenere prigionieri i propri utenti al fine di distribuire pubblicità e raccogliere loro dati personali. È il tipico esempio di Facebook che non permette ai propri utenti di comunicare con altri utenti non membri senza utilizzare gli strumenti di Facebook . Ma è anche il caso di Google/Alphabet insieme al suo servizio di video Youtube: certo potete sempre vedere video senza iscrivervi ma data la posizione preponderante che ha acquisito Youtube nel mondo, è diventato praticamente impossibile per altre organizzazioni proporre un servizio simile, per ragioni economiche ma anche perché Google/Alphabet a assunto una posizione egemonica.

Per padroneggiare, controllare e sorvegliare sempre più la circolazione delle informazioni nel web e rafforzare il proprio modello di business, gli attori egemoni sono giunti a concentrare anche una gran parte di Internet imponendo le proprie infrastrutture tecniche (server farm, servizi di hosting, e persino l’acquisizione di cavi sottomarini che collegano i continenti). 

## La neutralità

Una delle leve principali che permette di affrontare ciò che può equivalere a un blocco globale dell’informazione, è il concetto di neutralità della rete. È al contempo un principio e uno strumento giuridico: la neutralità della rete implica che un messaggio debba circolare allo stesso modo indipendentemente dal contenuto, dal mittente o dal ricevente. Cosa direste se una notizia dovesse essere sistematicamente rallentata o modificata in base alla tendenza politica del sito d’informazione che la diffonde o a seconda del proprietario dei canali attraverso i quali passa? O più semplicemente, lascereste che il vostro postino apra la vostra corrispondenza e selezioni lui stesso, secondo il contenuto,  cosa vi consegnerà in giornata e cosa riceverete diversi giorni più tardi?

Tali sono le ragioni che spingono oggi numerose organizzazioni a sostenere un (ri)decentramento di Internet, vale a dire mettere fine alle pratiche egemoniche delle principali aziende mondiali di servizi digitali e promuovere l’uso di protocolli aperti. È anche la possibilità di realizzazione di nuove forme di economia, basate in particolare sugli scambi tra pari. Ma attenzione, nuovi modelli di business che sfuggono a qualsiasi regolamentazione (specialmente quando utilizzano tecnologie del tipo blockchain) possono pure loro rappresentare una minaccia per le libertà concentrando a modo loro le competenze e le finanze. Se il software libero trova la propria forza nella pluralità delle iniziative, l'[etica](../Collaborare/etica.md) non è meno importante.


## La decentralizzazione di Internet è un problema della ESS

In questo contesto, l’Economia Sociale e Solidale parla più o meno la stessa lingua. La governance classica delle aziende e delle istituzioni è generalmente piramidale, con una forma di centralizzazione dell’informazione a scopo di controllo. In un modello cooperativo, al contrario, la solidità della struttura dipende da diversi livelli democratici di regolamentazione, il che conduce ad un decentramento degli organi decisionali. Ciò riflette un importante bisogno di autonomia digitale. Di fatto, sarebbe perfettamente controproducente ricentralizzare la decisione utilizzando i servizi Internet di un’unica società egemone: ciò equivarrebbe a mettere in pericolo i dati di tutti gli utenti, rendendo i processi decisionali dipendenti da un attore terzo la cui fiducia e affidabilità possono essere messe in dubbio, e ciò rimette pure in discussione l’autonomia digitale dell’organizzazione che li utilizza.


I valori della ESS, in particolare nel mondo associativo, vanno quasi naturalmente incontro alle pratiche digitali di mercato. Anche se non è sempre facile e anzi spesso è costoso realizzare sistemi di informazione e di comunicazione autonomi basati su software libero e opensource, è sempre più indispensabile farlo. Per questo si può seguire l’esempio di CEMÉA, con il progetto [Zourit](../collaborer/zourit.md).
{: .encart }

Dal momento che le competenze non sono sempre disponibili, è il grande principio di solidarietà che deve giocare al massimo: mettere in comune queste competenze tra organizzazioni della ESS, fare appello ad attori etici e solidali facenti parte pure loro della ESS, valutare la fiducia in tali attori etici,  diffondere le pratiche per incoraggiare il decentramento di Internet e l’autonomia di tutti.
