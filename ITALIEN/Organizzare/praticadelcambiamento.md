---
title: La pratica del cambiamento
---

Alcuni punti di riferimento che saranno di aiuto per una migrazione efficace verso il software libero.
{: .slogan-subtitle }

All’inizio di una transizione verso il software libero è importante porsi alcune domande. L’esperienza associativa può essere di aiuto, nella tabella qui sotto sono riassunti alcuni punti in base ad esperienze vissute da diverse associazioni, naturalmente la lista non è da ritenersi esaustiva.

----------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

| Iniziative                                                                             | Strategia |
| -------------------------------------------------------------------------------------- | --------- |
|Chi dirige l’iniziativa?                                                                 |  Sarà portata a termine? Meglio evitare di porre decisioni e responsabilità su di una sola persona a capo del progetto. Preferire un insieme di iniziative collettive a conoscenza dei problemi e uno o due coordinatori. |
| I futuri utenti, quanto sono coinvolti nelle decisioni? |   Le variabili che possono inibire o facilitare il passaggio al software libero sono sia individuali che collettive. |
| Quando proporre il progetto?                                                              |   Organizzate con cura il progetto prima di proporlo. Siate in grado di spiegarlo con cura e dettaglio per assicurarvi una collaborazione trasversale e redditizia. |
| Quando coinvolgere gli utenti?                                                |  Possono essere coinvolti in diverse fasi, non è necessario chiederglielo ogni volta. |
| Pianificare i periodi.                                                                   |   Adottare nuovi software non si fa in fretta e furia: organizzate, pianificate e adattate il calendario alla situazione reale. |

----------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Migration : contraintes collectives

|  Fattore                                                                  |  Azioni da intraprendere |
| ------------------------------------------------------------------------- | --------------- |
| Necessità di formazione sul software e sugli utilizzi (mancanza di tempo, organizzazione della formazione, ecc.)                      | Preparare livelli sufficienti di competenza degli utenti, pianificare l'adozione del software in base al tempo dedicato alla formazione, sfruttare un periodo di tranquillità nelle attività quotidiane dell'associazione. |
| Interoperabilità (compatibilità tra sistemi, gestione di formati per gli archivi, ecc.)    |    L'adozione di software libero non è solo un problema tecnico: bisogna valutare l'intera organizzazione della produzione di informazioni per minimizzare il rischio di conflitti tecnologici, per valutare la perdita accettabile di informazioni... |
| Informazioni insufficienti (le procedure diventano inefficaci).       | Prendetevi il tempo necessario per informare gli utenti (il che richiede una sufficiente competenza), spiegare le nuove procedure passo dopo passo, personalizzare le spiegazioni. |
| Disparità inerenti al software tra tutti i membri dell'associazione.       |  Insistere sulla nozione di interoperabilità, ma lasciare libertà di scelta agli utenti. Per esempio, i formati aperti possono essere usati per scambiare documenti senza costringere tutti ad usare GNU/Linux come sistema operativo. L'utilizzo di servizi online consente inoltre di render i contenuti indipendenti dai software installati localmente. |
| Mancanza di chiarezza sugli sviluppi futuri del software libero (quando spesso dipendono da una manciata o addirittura uno sviluppatore unico).                       |  Individuare correttamente i bisogni e valutarli in relazione alle costanti del software previsto (informarsi presso un'altra associazione che ha adottato lo stesso approccio, considerare gli aggiornamenti futuri, il possibile coinvolgimento della vostra associazione nello sviluppo del software...). |

------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Migrazione: vincoli dal punto di vista dell'utente

| Fattore   | Azione da intraprendere |
| --------- | -------------- |
| Insufficienze funzionali (osservate effettivamente o sentite solo a causa della mancanza di competenza dell'utente). |  Prima di considerare se scegliere un altro software (e quindi perdere tempo e produttività), è necessario coinvolgere gli utenti nella scelta: nominare dei "beta-tester", pianificare un periodo riservato alla sperimentazione. |
| Interoperabilità (compatibilità tra sistemi, gestione dei formati per gli archivi, ecc.) | Spiegare queste nozioni spesso astratte: richiedono nuove prassi  e gradi di empatia. Imparare a gestire la comparsa di un nuovo formato nel flusso di informazioni. |
| Differenze ergonomiche o logiche nell'uso, competenze ("Non posso fare quello che facevo prima, quindi questo software è un male"). | Rimettere in discussione le pratiche, comprese le vecchie pratiche di aggiramento dei vincoli: sviluppare un piano di comunicazione collettiva e di apprendimento tecnico, non lasciare mai un socio da solo di fronte a difficoltà tecniche. |
| Informazioni insufficienti ("Non so se posso farlo") o troppo direttive ("Non sono affidabile").   | Sviluppare un piano di comunicazione e di scambi collettivi, non lasciare mai un membro da solo di fronte a dubbi, confermare concretamente la legittimità di ogni membro a dare la propria opinione (creare spazi e tempi per questo). |
| Solitudine del socio volontario a casa (come adottare il software libero?) | Proporre eventi collettivi dedicati al software libero, incoraggiare l'aiuto reciproco tra i membri, creare un team di supporto (di persona, per telefono, in videoconferenza o aprendo una sessione a distanza sulla macchina). |
| La responsabilità per i fallimenti è di uno o pochi membri volontari ( sensazione di essere in una crociata perdente). | Le scelte strategiche devono sempre essere collettive e non affidarsi a una sola persona che se ne assuma la responsabilità. |
| Discorsi colpevolizzanti nei confronti dei non utenti di software libero (ritenuti corresponsabili per la mancanza di etica del software che utilizzano, o per le minacce alle libertà digitali, le invasioni della privacy). | Evitare fenomeni regressivi (negazione, ricerca di false giustificazioni, ostilità). Tenere un discorso di apertura, non catastrofico ed empatico. |

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Migrazione : fattori per l’adozione (dal punto di vista collettivo)

| Fattore | Punti a cui fare attenzione |
| ------- | ------------------- |
| Indipendenza tecnologica della struttura in relazione ai vincoli (finanziari, etici) dei software proprietari. | Valutare i nuovi vincoli di questa autonomia: competenze, spese per i fornitori esterni, (ad es. affitto del server) |
| La formazione o i cambiamenti nell’infrastruttura sono vissuti come degli investimenti. | Ogni passo indietro avrà un costo molto elevato, occorre perciò fare di tutto per evitarlo. Le nuove competenze degli utenti devono essere valorizzate e utilizzate e soprattutto non devono essere dimenticate o trascurate, ( altrimenti « a cosa serve? ») |
| Accoglienza positiva di una strategia di lungo periodo, sentimento di coinvolgimento da parte degli utenti. | Possibili eccessi dovuti all’entusiasmo, iniziative arrischiate. |
| Coinvolgimento degli utenti (gruppo) nel processo decisionale (sfida collettiva). | Saper organizzare le riunioni, moderare le discussioni. |
| I membri volontari si formano reciprocamente agli utilizzi. | Come organizzare il tempo di lavoro per lasciare spazio a queste sessioni di mutuo aiuto. |
| Passare dallo statuto di utente a quello di utente che contribuisce (azione: contribuire allo sviluppo del software, anche solo simbolicamente, programmazione, traduzioni, donazioni... | Del tempo di lavoro disponibile, mettere ai voti l’erogazione di una somma per gli sviluppatori, promuovere il software…
| Interoperabilità (« insomma, ci si capisce»). | Saper gestire l’interoperabilità o la sua assenza nelle relazioni esterne all’associazione. |

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Migrazione: fattori per l’adozione (dal punto di vista dell’utente)

| Fattore | Punti a cui fare attenzione |
| ------- | ------------------- |
| Utilizzo di software libero nella sfera privata. | Tutte le pratiche individuali non sono necessariamente le più adatte in ambito professionale. |
| Facilità d’utilizzo, ergonomia. | Una cattiva ergonomia deve essere oggetto di una grande attenzione, perché l’efficacia di un software non basta sempre a compensare il suo design insufficiente: è un fattore di rischio. |
| Interoperabilità: meno stress... | Decidere quali sono i formati adatti alla propria associazione. |
| Valorizzazione dell’utente (si richiede la sua competenza e il suo parere). | Ascoltare i feedback degli utenti.
| Comprensione dei vantaggi per il proprio lavoro e la propria associazione. | Mantenere lo sforzo collettivo. |
| Passare dallo statuto di utente a quello di utente che contribuisce  (azione: contribuire allo sviluppo del software, anche solo simbolicamente, programmazione, traduzioni, donazioni...) Riservare eventualmente del tempo di lavoro per questo compito. | Le azioni devono essere decise collettivamente.. |
| Acquisizione di nuove conoscenze. | Riservare del tempo alla formazione, valorizzare le conoscenze acquisite. |





