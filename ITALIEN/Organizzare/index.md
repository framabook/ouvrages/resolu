---
title: Organizzare
---

Un vero lavoro, una strategia
{: .slogan-subtitle }

L’organizzazione delle attività della vostra associazione o della vostra azienda è un’attività a sé stante. Affinché il tempo dedicatogli non invada quello che intendete dedicare alla realizzazione dei vostri progetti, è indispensabile attrezzarsi con strumenti che permettano un’organizzazione efficace.

Esistono numerosi strumenti per farvi guadagnare tempo e avere una visione più chiara della gestione dei vostri soci, dei vostri documenti, del vostro bdget, dei vostri eventi e dell’avanzamento delle vostre azioni.

## I buoni strumenti per organizzarsi in modo efficace

Si possono distinguere le applicazioni di gestione associativa semplice (gestione degli iscritti, contabilità) e le applicazioni che permettono di svolgere più compiti e simili a soluzioni di amminstrazione e di animazione. 

Le prime sono applicazioni specifiche per compiti precisi. Galette, per esempio, è un’applicazione dedicata alla gestione degli iscritti e alle liste di posta elettronica. Grisbi o Kresus sono applicazioni dedicate alla contabilità personale ma possono adattarsi molto bene ad una piccola associazione. Noalyss, dal canto suo, è un’applicazione online specializzata esclusivamente nella contabilità di associazioni o imprese.

Le seconde sono comunemente chiamate applicazioni di pianificazione delle risorse d’impresa (*Entreprise Resource Planning*, ERP). Tali ERP presentano numerosi vantaggi per un’associazione :

- quando sono specializzate per le associazioni, vedi Garradin o Diacamma, permettono la gestione amministrativa e finanziaria ma propongono anche strumenti online come la condivisione di documenti, dei Wiki, archiviazione, gestione di eventi, ecc.
- quando sono specializzate per l’impresa, vedi Odoo o Dolibarr, permettono la gestione del magazzino e dei clienti (o di iscritti e partecipanti), la gestione contabile, la gestione di progetto, il mailing e anche la gestione di siti web.

Sta a voi definire i vostri bisogni. Tuttavia una raccomandazione: nel mondo del software, l’adagio « chi può fare di più può fare anche di meno » non funziona… Di fatto, come sempre tutto dipende dalle competenze dell’utilizzatore. Inoltre è inutile imparare ad utilizzare un’applicazione ERP se avete bisogno di gestire qualche adesione e di fare un sito web. Valgono di più due applicazioni che si occupano di un compito ciascuno ma in maniera efficace e semplice. Al contrario, se la vostra associazione raccoglie migliaia di iscrizioni se organizzate grandi eventi, è importante formare il vostro personale nell’uso di applicazioni ERP.
{: .encart }

### Organizzare i propri documenti e i propri archivi

Per assicurare un’archiviazione chiara dei vostri dati e una classificazione dei vostri documenti in modo da renderli facilmente consultabili, avete tutto l’interesse ad utilizzare strumenti specializzati nell’archiviazione. I vantaggi sono di poter trovare rapidamente un documento necessario, di evitare di perderne, di costituire degli archivi in modo da distinguere i documenti obsoleti o attualizzati e condividere questo spazio di archiviazione con tutti i membri della vostra struttura. [Nextcloud](../Collaborare/nextcloud.md) è una suite di strumenti ospitata su di un server, che vi permette di organizzarvi in tal modo. La sua funzione principale consiste nell’archiviare e condividere files online; potete condividere i vostri documenti, i vostri calendari, compiti, contatti, ecc.

Una gestione precisa e semplice dei diritti di accesso permette di scegliere con quali membri condividere un file o una cartella. È anche possibile rendere pubblico un calendario  di eventi.

Per finire, [Nextcloud](../Collaborare/nextcloud.md) propone estensioni che possono essere attivate dall’amministratore. Esse permettono di aggiungere funzionalità come l’integrazione di sistemi di messaggeria, di sondaggi, di note, ecc… : a voi scegliere cosa corrisponde meglio ai vostri bisogni.


## Organizzarsi per tempo 

Poter disporre di agende condivise con i membri della vostra organizzazione consente numerosi vantaggi: vi permette di centralizzare in un solo posto le informazioni relative agli eventi, di sincronizzare i vostri appuntamenti e di modificare in maniera collaborativa i calendari della vostra associazione o della vostra azienda.

NextCloud Agenda è un servizio che offre tutte queste funzionalità, collegandole ad altre (gestione dei promemoria, dei contatti, ecc.).
Per organizzare i vostri appuntamenti, lo strumento Framadate ([framadate.org](http://framadate.org/)) vi propone di creare un sondaggio nel quale potete scegliere il grado di precisione di ciascuna scelta (giorni, ore…). Framadate permette a ciascun partecipante di selezionare ogni opzione con un grado di preferenza.

Infine Kanban (ovvero [kanboard.net](http://kanboard.net/)) permette di pianificare un progetto e di organizzare visivamente i compiti che lo compongono. Ciascun compito corrisponde a una mappa posizionata su di una tabella che permette di seguire l’avanzamento del progetto. I compiti possono essere assegnati a differenti membri, indicare una data di scadenza, essere scomposti in elenchi di sottocompiti… Soprattutto, la tabella è collaborativa: la persona incaricata della realizzazione di un compito lo sposta sulla tabella in maniera autonoma.



## Organizzarsi finanziariamente

Lo strumento di gestione contabile [Garradin](garradin.md) è noto per rispondere ai bisogni di piccole e medie associazioni, per soddisfare le proprie esigenze contabili. Si tratta di uno strumento tanto completo quanto semplice da usare, che vi permette di organizzare efficacemente il monitoraggio delle entrate e delle uscite, la contabilità in partita doppia e la produzione dei bilanci obbligatori. Organizzarsi con Garradin consente sia di guadagnare tempo che di assicurare alla prorpia struttura una gestione contabile completa e trasparente.

Allo stesso modo, con l’applicazione [Galette](galette.md), una buona organizzazione degli iscritti e delle loro contributi vi permette di vedere più chiaro nella ripartizione del vostro budget e nelle disponibilità umane e finanziarie.

Nato dall’esperienza delle associazioni, Galette è uno strumento molto utile per gestire le adesioni e le iscrizioni. Quest’applicazione funziona come un servizio online e permette una gestione collettiva (ciascun socio può aggiornare le informazioni che lo riguardano).


## Adottare nuove abitudini con strumenti liberi

Scegliendo soluzioni libere che rispondono più da vicino ad una particolare esigenza organizzativa, vi date tutte le possibilità di gestire più efficacemente la vostra attività.

Le soluzioni libere sono concepite pensando all’interoperabilità: i vostri dati restano sotto il vostro controllo e potete recuperarli, trasferirli e condividerli senza alcun ostacolo.
{: .slogan-contenu }

Realizzate in modo collaborativa, le soluzioni libere traducono i bisogni reali della comunità di utilizzatori; e se nonostante ciò vi manca una funzionalità, vi è possibile provare uno strumento alternativo o chiedere una implementazione alla comunità di sviluppatori.

[![cyberstructure](../img/cyberstructure.jpg){: .illustration }](https://cfeditions.com/cyberstructure/){target=_blank}
*[Stéphane Bortzmeyer](https://www.bortzmeyer.org/){target=_blank}    
[Cyberstruttura](https://cfeditions.com/cyberstructure/){target=_blank}  
L'Internet, uno spazio politico, 
C&F Éditions 2018*
{: .figure }
