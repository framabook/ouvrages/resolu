---
title: Scrittura collaborativa con Etherpad
---

Modificare documenti e prendere appunti insieme con Etherpad
{: .slogan-subtitle }

Etherpad è uno strumento di editing dei documenti online. Permette a più persone di lavorare sullo stesso testo contemporaneamente, facilitando così la collaborazione. Questo documento viene chiamato **pad**.

## Facile da usare

Offrendo le funzionalità di base di un word processor, Etherpad è intuitivo e non sconvolge le tue abitudini.

Per iniziare a lavorare basta creare un pad e condividere il link con i tuoi collaboratori, non è nemmeno necessario creare un account!

## Pratico

Ognuno dei partecipanti ha un colore per riconoscerli e sapere chi ha scritto cosa. Non preoccuparti, questi colori possono essere disabilitati e non vengono visualizzati nel documento finale.

I sistemi di commenti e chat istantanea  permettono di dare il proprio parere e discutere in diretta sul documento.

Inoltre, Etherpad mantiene una cronologia di tutto ciò che è stato digitato: nessun rischio di perdere il documento a causa di una operazione sbagliata!

## Funzionalità a scelta

Etherpad è un software gratuito, vale a dire che tutti possono installarlo e renderlo disponibile. Una volta installato e aperto online per il pubblico, diventa un'istanza (ad esempio il servizio Framapad dell'associazione Framasoft è un'istanza Etherpad). Ogni istanza può offrire funzionalità diverse.

L'aggiunta di funzionalità a Etherpad è facile con le estensioni. Ad esempio, aggiungi temi, opzioni di esportazione, caratteri o colori. Alcune istanze eliminano i pad dopo un certo periodo, altre suggeriscono di proteggere i pad con una password, ecc.

Queste opzioni devono essere attivate da chi ospita l'istanza, quindi è interessante testarne diverse per vedere quale si adatta meglio alle tue esigenze. 

## Qualche esempio di utilizzo

I pad possono essere utilizzati per soddisfare molte esigenze. Ad esempio, è possibile utilizzarli come strumento collaborativo per prendere appunti durante una riunione, come spazio per il brainstorming o come word processor di base per scrivere un documento.

## Adesso tocca a te

Molti membri del collettivo CHATONS offrono istanze Etherpad. Possiamo ad esempio citare:

-   [framapad.org](http://framapad.org/)

-   [pad.picasoft.net](http://pad.picasoft.net/)

-   [pad.colibris-outilslibres.org](http://pad.colibris-outilslibres.org/)

-   [pad.hadoly.fr](http://pad.hadoly.fr/)

-   [pad.chapril.org](http://pad.chapril.org/)

E altri su [chatons.org](http://chatons.org/) !



**Prima –** Creo un documento, lo invio a tutti per la modifica. Ricevo 5 versioni con delle aggiunte, passo mezz’ora a raccogliere il tutto e a fare una nuova versione che invio a tutte/i. Ricevo 2 correzioni, una versione con delle aggiunte e una risposta alla prima versione che è stata poi modificata. Passo un’ora a raccogliere tutto, ecc. Ecc.<br>
**Adesso –** Creo un pad, invio il link a tute/i e posso concentrarmi sui contenuti che si sviluppano con il contributo di tutte/i!
{: .encart }

## Buone pratiche

Ricordati di inserire il tuo nome o pseudonimo nel campo creato a questo scopo, per aiutare gli altri a identificarti.

I pad devono essere considerati come provvisori. Non dimenticarti di esportare i documenti che vuoi conservare.

Attenzione, in generale i pad sono pubblici. Non sono indicizzati (restano perciò invisibili ) ma  sono accessibili a chiunque abbia l’indirizzo. È perciò importante fare attenzione a non lasciarvi informazioni riservate o informazioni personali senza il permesso delle persone interessate.

[![libertes-numeriques](../img/libertes_numeriques.jpg){: .illustration }](https://framabook.org/libertes-numeriques/){target=_blank}
*[Christophe Masutti](https://golb.statium.link/){target=_blank}    
[Libertà Digitali](https://framabook.org/libertes-numeriques/){target=_blank} -
[[PDF ↓]](https://framabook.org/docs/dumo/dumo_couleur_licenceArtLibre_novembre2017.pdf){target=_blank}    
Guida di buone pratiche ad uso dei DuMo,   
Framabook 2017  
Licence [Art Libre](http://artlibre.org/){target=_blank}*
{: .figure }
