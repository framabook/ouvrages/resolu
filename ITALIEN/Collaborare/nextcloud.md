---
title: Condividere  documenti con Nextcloud
---

Sincronizza file, calendari, attività e  molto altro ancora!
{: .slogan-subtitle }

Nextcloud è una suite di strumenti su un server. Stiamo parlando di un'istanza Nextcloud. La sua funzione principale è quella di archiviare e condividere file online. Inoltre, consente di utilizzare diverse applicazioni complementari come se si trattasse di un desktop remoto.

Con Nextcloud puoi condividere documenti, calendari, attività, contatti, ecc. Niente più versioni diverse dello stesso documento perse nelle e-mail, niente più modifiche al programma delle riunioni che tutti dimenticano! Tutti hanno accesso alle stesse informazioni in ogni momento!

Grazie a una suite di applicazioni sviluppate dalla comunità, puoi approfittare delle numerose funzioni aggiunte: condivisione di mappe, note, ricette di cucina, moduli, foto, ecc.
{: .encart }

## Strumenti di collaborazione

Una gestione precisa e semplice dei diritti di accesso consente di condividere ciò che si desidera con chi si desidera. Ad esempio, è possibile condividere una cartella per lavorare con più persone sullo stesso progetto fornendo diversi livelli di accesso o rendere pubblico il calendario degli eventi organizzati dalla propria associazione.

Nextcloud offre anche molte estensioni che possono essere attivate dall'amministratore. Ti consentono di aggiungere funzionalità, ad esempio l'integrazione di sistemi di messaggistica, sondaggi, note, ecc \... al fine di soddisfare al meglio le tue esigenze. 

## Modifica dei documenti

Tra le estensioni offerte, ce ne sono diverse che consentono la modifica collaborativa dei documenti. Ad esempio, le suite LibreOffice o OnlyOffice possono essere integrate per modificare in diretta documenti di testo, presentazioni o persino fogli di calcolo.

## Sincronizzazione


L'interfaccia Web di Nextcloud consente di eseguire tutte le operazioni necessarie. Tuttavia, a volte è più semplice lavorare direttamente sui file sul computer o utilizzare l'applicazione di calendario sullo smartphone. Questo è del tutto possibile con Nextcloud, che offre la **sincronizzazione** supportata dalla maggior parte delle app che già usi.

La sincronizzazione è un processo che permette di far corrispondere dei file archiviati in più posizioni. Ad esempio puoi sincronizzare una cartella sul tuo computer, localmente, con la stessa cartella su Nextcloud e se la condividi con qualcuno, anche la stessa cartella sul loro computer verrà sincronizzata.  I dati di un **calendario** o dei **contatti** possono essere trattati allo stesso modo.



## Adesso tocca a te!

Puoi iniziare a usare Nextcloud ora! 

Alcuni CHATONS offrono spazio di archiviazione gratuito per iniziare a utilizzare il servizio  individualmente o per un test. Ad esempio, puoi visitare 
[Zaclys.com](http://Zaclys.com/), [TeDomum.net](http://TeDomum.net/), o uno dei tanti altri CHATONS  disponibili su
[chatons.org](http://chatons.org/) !

## Ma è gratuito?

Nextcloud è un software open source, è gratuito e può essere installato su un server. Tuttavia, rendere disponibile questo servizio richiede una certa capacità di archiviazione e della manodopera. Significa anche assumersi la responsabilità dell’hosting. Ogni host offre prezzi diversi che spesso cambiano in funzione dell'utilizzo.

## Buone pratiche

Per le cartelle condivise, ricorda che non sei l'unico a lavorarci sopra! Si consiglia di definire con tutte le persone che  vi hanno accesso  un'organizzazione dei file e delle cartelle. 

Come con qualsiasi archivio, non dimenticare di fare un **backup** altrove rispetto al cloud: non sai mai cosa può succedere! 

Non dimenticare di formare gli utenti all’uso dei i nuovi strumenti ([gestione del cambiamento](../Organizzare/gestionedelcambiamento.md)). Può essere utile individuare una persona di riferimento che sarà in grado di aiutare gli altri quando necessario. Non esitare a chiedere un aiuto esterno, per esempio agli  CHATONS!


[![infrastructures-numeriques](../img/infrastructures_numeriques.jpg){: .illustration }](https://framabook.org/sur-quoi-reposent-nos-infrastructures-numeriques/){target=_blank}
*[Nadia Eghbal](https://nadiaeghbal.com/){target=_blank}    
[Su cosa si basano le nostre infrastrutture digitali?](https://framabook.org/sur-quoi-reposent-nos-infrastructures-numeriques/){target=_blank}    
[[Version Web]](https://books.openedition.org/oep/1797){target=_blank}      
Il lavoro invisibile dei creatori del web, 
OpenEdition Press / Framabook 2017  
Licence [CC BY](https://creativecommons.org/licenses/by/3.0/){target=_blank}*
{: .figure }
