---
title: Videoconferenza con Jitsi Meet
---

Le riunioni a distanza diventano semplici ed efficaci con Jitsi Meet !
{: .slogan-subtitle }

Jitsi Meet è uno strumento di video/audioconferenza. Permette di fare riunioni a distanza, semplicemente cliccando su un link.

## Come funziona?

Sulla maggior parte degli strumenti abituali, è necessario creare un account indicando il proprio indirizzo e-mail o il numero di telefono per utilizzare il servizio. Con Jitsi Meet, niente di tutto questo. Da qualsiasi computer, e anche solo dal telefono, puoi partecipare a una conferenza. È inoltre possibile crearne  una dandole un nome e invitando i vostri collaboratori.

Se il link è aperto da uno smartphone, vi sarà offerto di scaricare l'applicazione. L'applicazione mobile è più ottimizzata  della versione browser, ma è anche possibile utilizzare Jisti sul vostro telefono con il browser, senza installare nulla.

Jitsi Meet è un software libero che utilizza una tecnologia abbastanza recente: **WebRTC**. I flussi non passano attraverso un server centrale, come nel caso di Skype o Adobe-connect che richiedono agli utenti di creare profili utente e registrando i messaggi. Jitsi, al contrario, permette il collegamento diretto tra i computer dei partecipanti alla videoconferenza, in forma **criptata** e senza registrarli.

## Pensato per lavorare

Jitsi offre alcune funzionalità molto utili in riunione: la possibilità di «alzare la mano» per chiedere la parola, uno spazio di chat, la condivisione dello schermo per mostrare documenti o proporre demo…

Grazie a queste funzioni le vostre riunioni possono svolgersi in maniera tranquilla e produttiva.

## Si integra con altri strumenti

La semplicità d’uso di Jitsi Meet permette d’integrarlo senza problemi con altri strumenti di collaborazione. Per esempio, le estensioni per   [Mattermost](mattermost.md) o [Nextcloud](../Collaborare/nextcloud.md) consentono di creare una videoconferenza direttamente senza dover cambiare strumento.

## Nulla da installare

E non è necessario nemmeno creare un proprio profilo utente! Il gruppo **condivide un collegamento** (un URL) che apre direttamente la videoconferenza. Si può anche aggiungere una password per assicurarsi che le solo le  persone invitate siano presenti.

## Adesso tocca a te!

Jitsi Meet è software libero. Può essere ospitato da chiunque. Esistono numerose istanze pubbliche che potete liberamente utilizzare, eccone alcune:

-   [meet.jit.si](http://meet.jit.si/)

-   [framatalk.org](http://framatalk.org/)

-   [talk.snopyta.org](http://talk.snopyta.org/)

-   [calls.disroot.org](http://calls.disroot.org/)

E per universitarie/ri una’istanza è messa a disposizione da RENATER su [rendezvous.renater.fr](http://rendezvous.renater.fr/).

In Italia, numerose istanze sono rese disponibili tramite il portale [iorestoacasa.work](https://iorestoacasa.work).

**Prima –** Convoco una riunione, alcuni membri sono fuori città, alcuni si prendono cura dei loro figli, ecc. Siamo in due alla riunione, gli altri membri non si sentono coinvolti, il mio progetto non procede.<br>
**Adesso –** Convoco una riunione online, tutti i membri che possono accedono, ovunque siano. I verbali sono scritti in modo collaborativo su un documento condiviso durante la riunione. La riunione va bene grazie agli strumenti di Jitsi e i partecipanti si sentono coinvolti, il mio progetto va avanti.
{: .encart }

Va anche notato che questo software è incluso nella lista dei software open source raccomandati dallo Stato francese come parte della modernizzazione globale dei suoi sistemi informativi (S.I.).

## Buone pratiche

Ricordatevi di pianificare un ordine del giorno in anticipo e di inviarlo ai partecipanti.

La qualità della tua videoconferenza dipende molto dalla tua connessione internet. Quando è possibile, preferite una **connessione via cavo** a una connessione wifi.

In uno scambio a due o tre la webcam e il microfono integrati nel vostro computer possono essere sufficienti. Se siete diversi membri presenti fisicamente in una stanza, si raccomanda l'uso di un microfono esterno omnidirezionale.

È anche preferibile utilizzare un videoproiettore o uno schermo grande in questi casi: l'uso di diversi computer nella stessa stanza può causare effetti di ritorno audio sgradevoli.

Ricordati di prevedere qualche minuto prima della conferenza per assicurarti che l'attrezzatura funzioni.
