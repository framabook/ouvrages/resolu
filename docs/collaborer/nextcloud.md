---
title: Partager ses documents avec Nextcloud
---

Synchronisez vos fichiers, agendas, tâches et bien plus encore !
{: .slogan-subtitle }

Nextcloud est une suite d'outils sur un serveur. On parle d'une instance
Nextcloud. Sa fonction principale consiste à stocker et partager des
fichiers en ligne. Il permet aussi d'utiliser différentes applications
complémentaires comme s'il s'agissait d'un bureau à distance.

Avec Nextcloud vous pouvez partager vos documents, vos calendriers, vos
tâches, vos contacts, etc. Finies les différentes versions d'un même
document perdues dans des courriels, finis les changements d'horaires de
réunion que tout le monde oublie ! Tout le monde a accès à la même
information à tout moment !

Grâce à une suite d'applications développées par la communauté, vous
pouvez aussi profiter de nombreuses fonctionnalités annexes : partager
de la cartographie, des notes, des recettes de cuisine, des formulaires,
des photos, etc.
{: .encart }

## Des outils de collaboration

Une gestion fine et simple des droits d'accès permet de partager ce que
l'on veut avec qui on veut. On peut par exemple partager un dossier pour
travailler à plusieurs sur un même projet tout en donnant différents
niveaux d'accès, ou bien rendre public le calendrier des événements
organisés par votre association.

Nextcloud propose également de nombreuses extensions pouvant être
activées par l'administrateur⋅rice. Elles permettent d'ajouter des
fonctionnalités, par exemple l'intégration de systèmes de messagerie, de
sondages, de notes, etc\... afin de coller au mieux à vos besoins.

## Édition de documents

Parmi les extensions proposées, il en existe plusieurs qui permettent
l'édition collaborative de documents. Par exemple, les suites
LibreOffice ou OnlyOffice peuvent être intégrées afin de modifier en
direct et à plusieurs des documents texte, diaporamas ou encore des
feuilles de calcul.

## Synchronisation

L'interface web de Nextcloud permet d'effectuer toutes les opérations
nécessaires. Cependant, il est parfois plus aisé de travailler
directement sur des fichiers sur son ordinateur, ou bien d'utiliser
l'application d'agenda de son smartphone. Cela est tout à fait possible
avec Nextcloud, qui offre une **synchronisation** prise
en charge par la plupart des applications que vous utilisez déjà.

La synchronisation est un procédé qui permet de faire correspondre des
fichiers stockés à plusieurs endroits. Par exemple vous pouvez
synchroniser un dossier sur votre ordinateur, en local, avec le même
dossier sur Nextcloud et si vous le partagez avec quelqu'un⋅e, le même
dossier sur son ordinateur sera aussi synchronisé. Les données d'un
**agenda** ou de **contacts** peuvent
être traitées de la même manière.

## À vous de jouer !

Vous pouvez commencer dès maintenant à utiliser Nextcloud !

Certains CHATONS proposent gratuitement un espace de stockage pour
commencer à utiliser le service dans un cadre personnel ou pour tester.
Vous pouvez par exemple aller voir du côté de
[Zaclys.com](http://Zaclys.com/), [TeDomum.net](http://TeDomum.net/), ou
un des nombreux autres CHATONS disponibles sur
[chatons.org](http://chatons.org/) !

## Et c'est gratuit ?

Nextcloud est un logiciel libre, il est gratuit et peut être installé
sur un serveur. Cependant, la mise à disposition de ce service demande
d'avoir une certaine capacité de stockage et de la main-d'œuvre. Cela
suppose aussi d'endosser la responsabilité de l'hébergement. Chaque
hébergeur propose des prix différents, souvent évolutifs en fonction des
usages.

## Bonnes pratiques

Pour les dossiers partagés, n'oubliez pas que vous n'êtes pas seul·e à
travailler dessus ! Il convient de définir avec toutes les personnes qui
y ont accès une organisation des fichiers et des dossiers.

Comme pour tout stockage, n'oubliez pas non plus d'effectuer des
**sauvegardes** ailleurs que sur le cloud : on ne sait
jamais ce qui peut se passer !

N'oubliez pas de **former** les personnes utilisatrices
aux nouveaux outils ([conduite du changement](../organiser/conduite-changement.md)).
Il peut être utile
de désigner une personne référente qui saura aider les autres en cas de
besoin. N'hésitez pas non plus à faire appel à de l'aide extérieure,
comme les CHATONS par exemple !

[![infrastructures-numeriques](../img/infrastructures_numeriques.jpg){: .illustration }](https://framabook.org/sur-quoi-reposent-nos-infrastructures-numeriques/){target=_blank}
*[Nadia Eghbal](https://nadiaeghbal.com/){target=_blank}    
[Sur quoi reposent   
nos infrastructures numériques ?](https://framabook.org/sur-quoi-reposent-nos-infrastructures-numeriques/){target=_blank}    
[[Version Web]](https://books.openedition.org/oep/1797){target=_blank}      
Le travail invisible des faiseurs du web   
OpenEdition Press / Framabook 2017  
Licence [CC BY](https://creativecommons.org/licenses/by/3.0/){target=_blank}*
{: .figure }
