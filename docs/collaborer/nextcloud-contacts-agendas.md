---
title: Nextcloud -- Contacts & Agenda
---

Synchroniser et partager des contacts : un outil stratégique
{: .slogan-subtitle }

On peut avoir un usage strictement personnel des applications Nextcloud,
mais l'intérêt de cette solution s'en trouve largement grandi lorsque
l'usage est collectif.

## Des applications

Les applications Contacts et Agenda sont des applications installées
d'emblée dans Nextcloud. Bien sûr, selon l'instance sur laquelle vous
avez un compte, elles seront plus ou moins accessibles. Tout dépend de
la manière dont les personnes chargées de l'administration de l'instance
ont configuré Nextcloud pour leurs utilisateur⋅rice⋅s.

De votre côté, c'est à vous qu'il revient de configurer sur votre
machine ou votre smartphone les applications qui pourront se connecter
aux applications de contact et d'agenda de votre instance Nextcloud. Le
principe consiste à synchroniser vos données de manière à ce que tous
vos dispositifs soient accordés.

## Synchroniser

La synchronisation se configure assez facilement sur de multiples
dispositifs. Elle s'applique à la gestion des contacts sur un
smartphone, la liste des contacts d'un client de courriel sur un
ordinateur, ou l'interface web de Nextcloud. De même, la gestion de
votre emploi du temps, de vos rendez-vous et toutes les invitations,
pourront indifféremment se gérer via vos appareils ou l'interface web.
Même les tâches peuvent se synchroniser (avec l'application Tâches de
Nextcloud ou OpenTasks sur smartphone).

Pour configurer tout cela, reportez-vous aux instructions d'utilisation
de votre instance ou du manuel Nextcloud. Pour l'essentiel, il s'agit de
se connecter via un protocole (WebDAV), utilisable sous n'importe quel
système d'exploitation.
{: .encart }

L'avantage de la synchronisation des contacts sur plusieurs appareils
consiste à ne pas restreindre l'usage de votre liste de contacts sur un
seul dispositif. Par ailleurs, l'erreur courante consiste à créer des
contacts dès que le besoin se présente, indépendamment du dispositif, ce
qui crée de nombreuses redondances et erreurs et rend difficile la
gestion des contacts.

Concernant l'agenda, il est bien utile de pouvoir l'avoir sous la main
lorsque vous utilisez votre PC comme lorsque vous êtes en déplacement et
que vous avez uniquement votre smartphone. Dans l'un ou l'autre cas,
vous savez que vos informations seront les mêmes sans avoir à les
recopier ou connecter « manuellement » vos appareils.

## Collaborer

À partir d'un compte, vous pouvez synchroniser vos propres contacts, les
garder pour vous, ou créer une liste de contacts que vous pouvez
partager avec vos collaborateur⋅rice⋅s. Vous pouvez aussi utiliser votre
session avec votre agenda et partager uniquement certains événements.

Nextcloud fonctionne avec une gestion fine des droits d'accès. Il est
donc possible, dans une équipe, non seulement de partager une liste de
contacts, mais aussi de déterminer qui a accès à telle liste de contacts
ou telle autre. Chaque modification sera synchronisée entre tous les
membres de l'équipe : adieu les contacts incomplets, les oublis, les
petits bouts de papier perdus. La gestion de l'agenda obéit aux mêmes
principes, en particulier lorsque contacts et agenda interagissent :
gérer les congés, organiser des réunions, créer des invitations,
partager un agenda public, organiser des permanences.

Tout cela permet à une équipe de maîtriser l'emploi du temps sans avoir
à multiplier les échanges par messagerie.

## Bonnes pratiques

Lorsque vous collaborez avec d'autres personnes, n'oubliez pas de penser
que tou⋅te⋅s vos collaborateur⋅rice⋅s n'attribuent pas toujours la même
pertinence à chaque information. Si vous organisez une réunion, son
objet, ses horaires et les noms des participant⋅e⋅s sont trois
informations différentes. De même, une fiche contact peut contenir de
multiples informations sur une personne (nom, prénom, numéro de
téléphone personnel ou professionnel, adresse courriel personnelle ou
professionnelle, adresse postale, etc.). Chaque information n'a pas
obligatoirement vocation à être connue de tou⋅te⋅s.

Ainsi, lorsque vous partagez des contacts ou un agenda, veillez à ce que
les autres utilisateur⋅rice⋅s obtiennent un niveau d'information
adéquat : le choix de tout partager indifféremment par souci de rapidité
peut-être lourd de conséquences. Nextcloud permet de gérer correctement
des droits d'accès individuellement ou par groupe via l'interface web et
de manière très facile. Profitez de ces fonctionnalités précieuses.

