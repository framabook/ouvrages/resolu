---
title: Communiquer
---

Comment faciliter la communication interne ou externe ?
{: .slogan-subtitle }

Dans toute organisation, les personnes collaboratrices ont besoin de
communiquer entre elles et avec le public. Il existe de nombreux outils
numériques pour communiquer, mais tous ne sont pas adaptés à toutes les
situations. Il est important de connaître les différentes solutions afin
de choisir les plus adéquates.

Que ce soit en **interne** avec les membres de sa
structure, ou en **externe** avec le public ou d'autres
organisations, il faut savoir changer d'outil selon les objectifs visés.
Les outils numériques ont révolutionné nos pratiques depuis plus de
trente ans. Pour s'organiser, collaborer, se faire connaître, etc., la
maîtrise de ces outils est fondamentale. Mais ces derniers ne sont pas
neutres et mal les choisir peut compromettre la **confidentialité des
données** échangées, contraindre des personnes à
exposer leur vie privée ou consommer bien plus de ressources que
nécessaire.

Facebook, par exemple, est un outil efficace lorsqu'il s'agit de
communiquer avec un public dont on sait évaluer la présence sur ce média
social. Pour autant, en faire l'unique canal de communication externe
oblige ceux et celles qui n'utilisent pas ce service à devoir s'y
inscrire. Ce faisant, au lieu de se contenter d'afficher des
informations et des liens, un effet réseau va se jouer. Cela rendra
dépendant⋅e⋅s des outils de Facebook aussi bien vos collaborateur⋅rice⋅s
que votre public.
{: .encart }

Des bulles artificielles empêchent de toucher de nouveaux publics. Elles
rendent très floues les frontières que vos collaborateur⋅rice⋅s doivent
entretenir entre leur travail et leur vie privée. Et ce n'est qu'un
aperçu d'une dépendance orchestrée malgré vous.
{: .slogan-contenu }

Cet ensemble de fiches propose quelques outils pratiques et éthiques
pour vous aider à mieux communiquer.
{: .text-center }

## La communication externe

Être visible sur Internet est très important afin de toucher le plus de
personnes possible.

Trois outils peuvent être utiles pour accroître votre visibilité : un
site web, les médias sociaux, et les courriels. Mais les bonnes
pratiques concernant ces outils ne sont pas toujours évidentes. Les
fiches dédiées à la communication externe mettront en avant les bonnes
pratiques avec ces outils, notamment en termes de respect de la vie
privée du public, et de cohérence par rapport aux thèmes abordés.

## La communication interne

Tous les outils de communication ne sont pas adaptés au travail en
équipe. Certains sont pensés pour des discussions personnelles entre
amis, d'autres pour des correspondances longues sur un sujet, d'autres
encore pour des discussions instantanées en équipe.

Nous aborderons aussi le sujet de la
[confidentialité](confidentialite.md) : lorsque l'on communique
entre collaborateur⋅rice⋅s, personnes bénévoles ou professionnel⋅le⋅s,
il est fréquent d'échanger des informations qui devraient rester
secrètes. Il est donc d'autant plus important d'utiliser un outil de
confiance et de faire attention aux destinataires des messages
« sensibles ». Une première façon de résoudre ce problème consiste en
l'adoption d'outils permettant de communiquer par groupe de travail.

[Mattermost](mattermost.md) permet par exemple d'échanger en équipe, en privé ou en
public -- et comme c'est un logiciel libre, le code est accessible et
audité publiquement, de sorte que l'on s'apercevrait bien vite si ce
programme contenait des failles permettant à des tiers indésirables
d'exploiter les messages.
{: .encart }

[![stand-mozilla](../img/stand_mozilla.jpg){: .illustration }](https://commons.wikimedia.org/wiki/Category:Lyon_JDLL_2018#/media/File:2018-03_JDLL_Stande_Wikimedia_France_01.jpg){target=_blank}
*De nombreux événements dédiés au logiciel  
libre sont organisés chaque année un peu  
partout. N'hésitez pas à vous y rendre !   
Stand de Wikimédia France (tenu par des  
bénévoles des projets Wikimedia)   
aux Journées du logiciel libre 2018  
de Lyon, France.  
Source: [Wikimedia](https://commons.wikimedia.org/wiki/Category:Lyon_JDLL_2018#/media/File:2018-03_JDLL_Stande_Wikimedia_France_01.jpg){target=_blank}  
Photo : Antoine Lamielle      
Licence [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/){target=_blank}*
{: .figure }
