---
title: Organiser
---

Un vrai travail, une stratégie
{: .slogan-subtitle }

L'organisation des activités de votre association ou de votre
entreprise est une activité à part entière. Pour que le temps qui lui
est dédié n'empiète pas sur celui que vous souhaitez allouer à la
réalisation de vos projets, il est indispensable de s'équiper d'outils
permettant une organisation efficace.

De nombreux outils existent pour vous faire gagner du temps et avoir une
vision plus claire de la gestion de vos membres, de vos documents, de
votre budget, de vos événements et de l'avancement de vos actions.

## Les bons outils pour s'organiser efficacement

On peut distinguer les logiciels de gestion associative simple (gestion
des adhérent⋅e⋅s, comptabilité) et les logiciels qui permettent
d'accomplir plusieurs tâches et s'apparentent à des solutions
d'administration et d'animation.

Les premiers sont des logiciels restreints à des tâches précises qu'ils
accomplissent exclusivement. Le logiciel Galette, par exemple, est
consacré à la gestion des adhérent⋅e⋅s et au mailing. Grisbi ou Kresus
sont des logiciels dédiés à la comptabilité personnelle mais peuvent
très bien s'adapter à une petite association. Noalyss, quant à lui, est
un logiciel en ligne spécialisé exclusivement dans la comptabilité
associative ou d'entreprise.

Les seconds sont communément appelés des logiciels de Planification des
ressources de l'entreprise (Entreprise Resource Planning, ERP). Ces ERP
présentent de multiples avantages pour une association :

-   lorsqu'ils sont spécialisés pour les associations, tels [Paheko](paheko.md) ou
    Diacamma, ils permettent la gestion administrative et financière
    mais proposent aussi des outils en lignes comme le partage de
    fichiers, des Wiki, de l'archivage, de la gestion d'événements,
    etc.

-   lorsqu'ils sont spécialisés pour l'entreprise, tels Odoo ou
    Dolibarr, ils permettent la gestion de stock et de clientèle (ou
    d'adhérent⋅e⋅s et participant⋅e⋅s), la gestion comptable, la
    gestion de projet, le mailing et même des sites web.

C'est à vous de définir vos besoins. Néanmoins une mise en garde : en
matière de logiciel l'adage « qui peut le plus peut le moins » ne
s'applique pas... En effet, tout dépend toujours des compétences de
l'utilisateur⋅rice. Aussi il est inutile d'apprendre à vous servir
d'un logiciel ERP si vous n'avez besoin que de gérer quelques
adhésions et de faire un site web. Mieux vaut deux logiciels qui ne
feront qu'une tâche chacun, mais de manière efficace et simple.
Inversement, si votre association comporte des milliers d'adhérent⋅e⋅s,
que vous organisez des événements de grande ampleur, il est important de
former votre personnel aux logiciels ERP.
{: .encart }

### Organiser ses documents et ses archives

Afin d'assurer un archivage clair de vos données et une classification
de vos documents pour les rendre consultables aisément, vous avez tout
intérêt à utiliser un outil spécialisé dans le stockage de documents.
Les avantages d'un tel outil sont de pouvoir retrouver rapidement un
document recherché, éviter d'en perdre, constituer des archives afin de
distinguer les documents obsolètes ou actualisés et partager cet espace
de stockage avec tou⋅te⋅s les membres de votre structure. [Nextcloud](../collaborer/nextcloud.md) est
une suite d'outils hébergés sur un serveur, qui vous permet de vous
organiser ainsi. Sa fonction principale consiste à stocker et partager
des fichiers en ligne ; vous pouvez partager vos documents, ainsi que
vos calendriers, vos tâches, vos contacts, etc.

Une gestion fine et simple des droits d'accès permet de choisir avec
quel⋅le⋅s membres partager un fichier ou un dossier. Il est aussi
possible de rendre public un calendrier d'événements, par exemple.

Enfin, [Nextcloud](../collaborer/nextcloud.md) propose de nombreuses extensions
pouvant être activées par l'administrateur⋅rice. Elles permettent
d'ajouter des fonctionnalités, comme l'intégration de systèmes de
messagerie, de sondages, de notes, etc\... : vous choisissez ce qui
correspond le mieux à vos besoins.

## S'organiser dans le temps 

Disposer d'agendas partagés avec les membres de votre organisation a de
multiples avantages : cela vous permet de centraliser les informations
relatives aux événements en un seul endroit, de synchroniser vos
rendez-vous et d'éditer collaborativement le calendrier de votre
association ou de votre entreprise.
[NextCloud Agenda](../collaborer/nextcloud-contacts-agendas.md) est un service proposant
toutes ces fonctionnalités, en les connectant à d'autres (gestion de
rappels, des contacts, etc.).

Pour organiser vos rendez-vous, l'outil Framadate
([framadate.org](http://framadate.org/)) vous invite à créer un sondage
pour lequel vous pouvez choisir le degré de précision (en jours,
heures...) de chaque choix. Framadate permet à chaque personne
 participante de sélectionner chaque option souhaitée avec un degré de
préférence.

Enfin, Kanban (voir [kanboard.net](http://kanboard.net/)) permet de
planifier un projet et d'organiser visuellement les tâches qui le
composent. Chaque tâche correspond à une carte placée sur un tableau
permettant de suivre l'avancement du projet. Les tâches peuvent être
assignées à des membres, indiquer une date d'échéance, être décomposées
en listes de sous-tâches... Surtout, le tableau est collaboratif : la
personne en charge du traitement d'une tâche la déplace sur le tableau
de manière autonome.

## S'organiser financièrement

L'outil de gestion comptable [Paheko](paheko.md) est conçu pour répondre aux
besoins des petites et moyennes associations, pour assumer leurs
obligations comptables. C'est un outil à la fois complet et simple
d'utilisation, qui vous permet d'organiser efficacement le suivi des
recettes et des dépenses, la comptabilité à double-entrée et la
production des bilans obligatoires. S'organiser avec Paheko, c'est à
la fois gagner du temps et assurer pour sa structure une gestion
comptable complète et transparente.

De la même manière, avec le logiciel [Galette](galette.md), une bonne organisation des
adhérent⋅e⋅s et de leurs cotisations vous permet d'y voir plus clair
dans la répartition de votre budget et dans les moyens humains et
financiers disponibles. Conçu à partir de l'expérience des
associations, Galette est un outil très utile pour gérer les adhésions
et les cotisations. Ce logiciel fonctionne comme un service en ligne et
permet une gestion collective (chaque membre peut mettre à jour les
informations le⋅la concernant).

## Adopter de nouvelles habitudes avec des outils libres

En choisissant des solutions libres qui répondent au plus près à un
besoin particulier d'organisation, vous vous donnez toutes les chances
d'organiser plus efficacement votre activité.

Les solutions libres sont conçues dans un souci d'interopérabilité :
vos données restent sous votre contrôle et vous pouvez les récupérer,
les transférer et les partager sans obstacle.
{: .slogan-contenu }

Réalisées de manière contributive, les solutions libres traduisent les
besoins réels de la communauté d'utilisateur⋅rice⋅s ; et si malgré cela
une fonctionnalité vous manque, il vous est possible d'essayer un outil
alternatif ou de demander une amélioration à la communauté de
développeur⋅se⋅s.

[![cyberstructure](../img/cyberstructure.jpg){: .illustration }](https://cfeditions.com/cyberstructure/){target=_blank}
*[Stéphane Bortzmeyer](https://www.bortzmeyer.org/){target=_blank}    
[Cyberstructure](https://cfeditions.com/cyberstructure/){target=_blank}  
L'Internet, un espace politique  
C&F Éditions 2018*
{: .figure }
