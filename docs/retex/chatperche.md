---
title: Dégooglisation : L'Atelier du Chat Perché
---

La mécanique vélo solidaire, c'est aussi des convictions
{: .slogan-subtitle }



*L'[Atelier du Chat Perché](http://www.chatperche.org/) est un atelier vélo participatif qui propose des permanences conviviales de mécanique vélo à ses adhérent·e·s. Lors des permanences, les adhérent·e·s peuvent venir entretenir et apprendre à réparer leurs bicyclettes. L'atelier vend aussi des vélos d'occasion qui ont été entièrement révisés. L'association participe à de nombreux événements dans Lyon et aux alentours, avec ses [vélos bizarres](http://www.chatperche.org/animations-velos-bizarres/), et propose des [ateliers mobiles d'initiation et de formation à la mécanique vélo](http://www.chatperche.org/ateliers-mobiles/) pour tous publics et toutes structures (écoles, centres sociaux, MJC, collectivités, entreprises). Nous avons demandé à l'un de ses membres de nous parler de sa démarche de passage à des outils libres.*

(Interview paru sur le Framablog le 09/09/2020)


— **Bonjour, peux-tu te présenter ? Qui es-tu ? Quel est ton parcours ?**

Bonjour, je suis Quentin. Dans le civil, je suis professeur des écoles. Je suis
adhérent et bénévole à l'Atelier du Chat Perché depuis 2015. Entre 2016
et 2019, j'ai été membre de la collégiale qui administre l'association
et j'ai pris en charge et coordonné la démarche d'autonomisation
numérique de l'association. Je n'ai aucune formation en informatique.
Je suis un simple sympathisant libriste, sans compétence particulière.
Je me considère plutôt comme un utilisateur averti.

— **Tu nous parles de ton association ?**

« Mécanique vélo et Autogestion » sont les mots inscrits sur le logo de l'association. L'Atelier du Chat Perché est donc un atelier de mécanique vélo où l'on vient pour entretenir soi-même sa bicyclette. En mettant des outils à disposition et en proposant une aide bénévole, l'association vise l'autonomie des
cyclistes dans l'entretien de leur moyen de transport. L'association compte 3 salariées et une grosse
vingtaine de bénévoles y tient les permanences de mécanique. L'Atelier est géré en collégiale et vise
une organisation autogérée.

C'est donc un collectif de gestion qui gère l'association en se réunissant une fois par mois pour prendre toutes les décisions. Des commissions et des groupes de travail gèrent des missions particulières et/ou ponctuelles. Une équipe bénévole accueille et organise les permanences de mécanique vélo. Les membres de l'organisation (salarié·es et bénévoles) sont plutôt à l'aise avec le numérique, bien qu'il y ait des différences d'un individu à l'autre.

— **Avant de vous lancer dans cette démarche de dégooglisation, vous utilisiez quels outils / services numériques ?**

L'Atelier du Chat
Perché proposait un site web hébergé chez Free comme principal moyen de
communication externe et utilisait des listes de diffusion (hébergées
elles aussi chez Free) pour les échanges entre les membres du collectif
de gestion, entre les bénévoles et pour communiquer des informations aux
adhérent·es. Nous utilisions aussi quelques Framapad, Framadate et
Framacalc pour nous organiser. Pour ce qui est de sa présence sur les
réseaux sociaux, l'atelier du Chat Perché n'a jamais utilisé Facebook.
Son utilisation a parfois été proposée puis discutée mais toujours
rejetée. Les valeurs portées par l'association et celles de Facebook
semblent ne pas être compatibles. En revanche, en plus de son
infolettre, l'asso utilise les réseaux sociaux
[diaspora](https://framasphere.org/people/334a2580f4f90134e64f2a0000053625)
et [mastodon](https://framapiaf.org/@chatperche) pour diffuser
automatiquement les publications de son blog. A l'époque, nous
n'avions pas de connexion Internet dans l'atelier. Les services
tournaient plutôt bien mais posaient deux problèmes principaux. Tout
d'abord, nous étions face à une incohérence : les valeurs de
l'association prônent l'autogestion et on avait confié l'hébergement
de nos services à un acteur privé. Et puis la gestion de ces outils
était totalement centralisée : une seule personne en avait la charge.

— **Vous avez entamé  une démarche en interne pour migrer vers des outils numériques plus éthiques. Qu'est-ce qui est à l'origine de cette démarche ?**

La première motivation était d'être davantage en cohérence
avec les valeurs de l'association. Il était évident qu'il nous fallait
migrer notre site web et nos listes de diffusion pour ne pas dépendre
d'un acteur tel que Free. À titre personnel, j'étais aussi très motivé
par cette démarche parce qu'elle allait me permettre d'apprendre à
mettre de nouveaux outils en place. J'allais donc enrichir mon
expérience et mes compétences, ce qui est totalement en accord avec les
valeurs de l'association. Nous avons donc lancé plusieurs chantiers :

- mettre en place un nouveau site web afin qu'il soit plus facile à prendre en main par les bénévoles ;
- réorganiser les contenus d'information qui y étaient publiés pour que ce soit plus clair ;
- utiliser un service d'auto-hébergement dans la mesure du possible ;
- transférer certains services (listes de diffusion) ;
- mettre en place d'autres services (mail, partage de fichiers, infolettres).

— **Quels sont les moyens humains qui ont été mobilisés sur la démarche ?**

Une personne bénévole, en l'occurrence moi-même, ayant peu de
compétences techniques mais une expérience d'utilisateur averti, a été
chargée de la coordination et de l'administration des services. J'ai
fait appel au support [bicloud de l'Heureux
Cyclage](https://www.heureux-cyclage.org/outils-mutualises.html) (une
sorte de [chatons](https://chatons.org/) pour le réseau des ateliers
vélos) lorsque nous avons eu besoin de compétences techniques. Migrer
tous nos outils sur des services libres nous a pris entre deux et trois
mois.

— **Peux-tu expliciter les différentes étapes de cette démarche ?**

Tout a commencé par une
présentation du projet à l'association. La collégiale m'a ensuite
chargé de coordonner le projet et m'a transmis toutes les informations
nécessaires pour cela (accès aux outils). Conscient de la limite de mes
compétences techniques, je me suis rapidement rapproché du bicloud pour
me renseigner sur les services proposés. C'est en échangeant avec eux
que j'ai pu choisir les outils les plus adaptés aux services numériques
dont nous avions besoin. Par exemple, il a fallu choisir quel
[CMS](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu)
nous souhaitions utiliser pour notre site web. En revanche, pour les
listes de diffusion, le bicloud proposait uniquement le logiciel
[Sympa](https://fr.wikipedia.org/wiki/Sympa_(informatique)) et pour le
stockage/partage de fichiers, uniquement le logiciel
[Sparkleshare](https://en.wikipedia.org/wiki/SparkleShare). Les
instances de ces logiciels ont été installées par l'équipe du bicloud.
Je n'ai alors plus eu qu'à les configurer.

— **Avez-vous organisé un accompagnement des utilisateur⋅ices pour la prise en main de ces nouveaux outils ?**

Oui, une fois les outils installés et configurés, il
a été nécessaire de former les membres du collectif de gestion afin
qu'iels puissent publier en autonomie des contenus sur le site web et
rédiger l'infolettre. Pour cela, on a proposé des rendez-vous
individuels à celleux qui le souhaitaient afin de leur faire la
démonstration des nouveaux outils. Et on a aussi programmé un temps de
présentation avec questions-réponses pour 4/5 personnes intéressées.
Mais je fais le constat que globalement les personnes ne viennent pas
bénévoler dans un atelier de mécanique vélo pour gérer des outils
informatiques. Il est donc difficile de recruter des bénévoles pour ces
tâches qui peuvent paraître loin du cœur de l'activité. D'ailleurs,
certaines personnes ont fait un pas de côté et ne se sont pas investies
dans les outils informatiques. Mais d'autres se sont remonté les
manches et ont pris la mission informatique en main lorsque j'ai quitté
le collectif de gestion. Ces personnes ont même déployé d'autre
services par la suite (remplacement du Sparkleshare par un Nextcloud).

— **Quelles difficultés / freins avez-vous rencontrées ?**

On a rencontré pas mal de freins : des Cantilever, des V-Brake, des freins à disques, à
tambour, à rétropédalage... Plus sérieusement, le frein le plus
important, ça a été de maintenir la continuité de ces services. Il est
en effet essentiel de penser l'après afin que la personne qui a lancé
le processus d'installation des services puisse rapidement transmettre
ses compétences à d'autres personnes. Le risque, c'est qu'une fois
que les services sont installés, le collectif (qui a d'autres chats à
fouetter) s'appuie complètement sur une seule personne pour les faire
tourner au quotidien. Et cela peut devenir usant pour ce bénévole qui
devient alors indispensable à l'utilisation de ces nouveaux outils.

— **Et l'avenir ? L'association envisage-t-elle de continuer cette démarche pour l'appliquer à d'autres aspects de son organisation ?**

L'association envisage de se doter d'un outil pour communiquer en
interne (pour le moment on utilise le mail). On a utilisé le logiciel
loomio via le service [Framavox](https://framavox.org/) pendant un
temps, mais peut-être faudrait-il qu'on teste le logiciel Mattermost
([Framateam](https://framateam.org/)). On verra aussi en fonction de ce
que propose le bicloud.

— **Quels conseils donneriez-vous à d'autres organisations qui se lanceraient dans la même démarche ?**

Le premier
conseil, ce serait de prendre son temps et d'accepter que ça prenne du
temps. Il vaut mieux y aller au fur et à mesure, un ou deux services à
la fois. Il est essentiel de réaliser un diagnostic des outils et de
leurs usages à un temps T et de s'interroger sur ce qui est réellement
utile ou inutile afin de pouvoir définir ensemble ce que l'on veut. Il
convient aussi de planifier et prévoir la transmission des connaissances
en terme d'utilisation et d'administration des services. Et puis, ça
peut être bien de ne pas laisser une seule personne être en charge de la
sélection et de l'installation des outils, mais de travailler à
plusieurs.

— **Le mot de la fin, pour donner envie de migrer vers les outils libres ?**

Il me semble nécessaire que les associations
militantes prennent le temps de mettre en cohérence leurs valeurs et
leurs pratiques numériques. Si les associations militantes ne font pas
le saut dans le libre, qui va le faire ? Choisir le numérique libre et
quitter les GAFAM, c'est comme choisir la bicyclette et se débarrasser
de sa bagnole : c'est faire le choix de la résilience, de la
convivialité et de l'émancipation.


